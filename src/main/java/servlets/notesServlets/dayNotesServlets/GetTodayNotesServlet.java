package servlets.notesServlets.dayNotesServlets;


import com.google.gson.Gson;
import model.Note;
import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "GetTodayNotesServlet", urlPatterns = "/today")
public class GetTodayNotesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idUser = Integer.parseInt(req.getSession().getAttribute("id").toString());

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.sortNotesOnDate(idUser);

        ArrayList<Note> todayNotes = notesDAO.getTodayNotes();

        Gson gson = new Gson();
        String todayNotesJSON = gson.toJson(todayNotes);

        if (todayNotesJSON != null) {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(todayNotesJSON);
        }
    }
}
