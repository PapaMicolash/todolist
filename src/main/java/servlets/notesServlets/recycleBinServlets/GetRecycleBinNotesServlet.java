package servlets.notesServlets.recycleBinServlets;

import com.google.gson.Gson;
import model.Note;
import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;



@WebServlet(name = "GetRecycleBinNotesServlet", urlPatterns = "/reBin")
public class GetRecycleBinNotesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String status = "DELETED";
        int idUser = Integer.parseInt(req.getSession().getAttribute("id").toString());

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.setRecycleBin(notesDAO.getNotesByIDUser(idUser, status));

        ArrayList<Note> recycleBin = notesDAO.getRecycleBin();

        Gson gson = new Gson();
        String recycleBinJSON = gson.toJson(recycleBin);

        if (recycleBinJSON != null) {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(recycleBinJSON);

        }
    }
}
