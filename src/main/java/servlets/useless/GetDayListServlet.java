package servlets.useless;

import com.google.gson.Gson;
import model.Note;
import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet(name = "GetDayListServlet", urlPatterns = "/getDay")
public class GetDayListServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idUser = Integer.parseInt(req.getSession().getAttribute("id").toString());
        String day = req.getSession().getAttribute("id").toString();

        ArrayList<Note> notesDay = new ArrayList<>();
        NotesDAO notesDAO = new NotesDAO();
        notesDAO.sortNotesOnDate(idUser);

        switch(day) {
            case "TODAY":
                notesDay = notesDAO.getTodayNotes();
                break;
            case "TOMORROW":
                notesDay = notesDAO.getTomorrowNotes();
                break;
            case "SOMEDAY":
                notesDay = notesDAO.getSomedayNotes();
                break;
            case "REBIN":
                notesDay = notesDAO.getRecycleBin();
                break;
            default:
                notesDay = null;
                break;
        }

        Gson gson = new Gson();
        String notesJSON = gson.toJson(notesDay);

        if (notesJSON != null) {
            System.out.println("ALLL is here!!!");
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(notesJSON);
        }
        else {
            System.out.println("errorrr");
        }
    }
}
