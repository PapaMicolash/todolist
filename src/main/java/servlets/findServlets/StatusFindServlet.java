package servlets.findServlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


@WebServlet(name = "StatusFindServlet", urlPatterns = "/findStatus")
public class StatusFindServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(req.getInputStream()));
        String json = "";

        if(br != null){
            json = br.readLine();
        }


        if (json != null) {
            req.getSession().setAttribute("noteStatus", json);
        }

    }
}
